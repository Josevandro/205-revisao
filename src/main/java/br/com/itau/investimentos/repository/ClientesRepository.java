package br.com.itau.investimentos.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.itau.investimentos.model.Clientes;

@Repository
public interface ClientesRepository extends CrudRepository<Clientes, Long> {
//	Clientes findByCpf(String cpf);
}
