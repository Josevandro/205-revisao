package br.com.itau.investimentos.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.itau.investimentos.model.AplicacaoDTO;
import br.com.itau.investimentos.model.Produto;

@Repository
public interface ProdutoRepository extends CrudRepository<Produto, Long>{
	
//	Iterable<AplicacaoDTO> findAllByProduto_Id(Long id);
}

