package br.com.itau.investimentos.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.itau.investimentos.model.Aplicacao;
import br.com.itau.investimentos.model.AplicacaoDTO;

@Repository
public interface AplicacaoRepository extends CrudRepository<Aplicacao, Long>{

//	Iterable<AplicacaoDTO> findAllByClientes_Id(Long id);
//	
//	Iterable<AplicacaoDTO> findAllByProduto_Id(Long id);
}
