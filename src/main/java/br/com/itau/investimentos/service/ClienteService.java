package br.com.itau.investimentos.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.investimentos.model.Clientes;
import br.com.itau.investimentos.repository.ClientesRepository;

@Service
public class ClienteService {
	
	@Autowired
	private ClientesRepository clientesRepository;
	
	public void cadastrarCliente(Clientes cliente) {
		clientesRepository.save(cliente);
	}
	
	public Iterable<Clientes> consultarClientes() {
		return clientesRepository.findAll();
	}
	
	public void deletarCliente(Long id) {
		Optional<Clientes> optional = clientesRepository.findById(id);
		
		if(optional.isPresent()) {
			clientesRepository.delete(optional.get()); 	
		}
	}
	
	public void editarCliente (Long id, Clientes clienteAtualizado) {
		Optional<Clientes> optional = clientesRepository.findById(id);
				
		if(optional.isPresent()) {
			Clientes cliente = optional.get();
			cliente.setNome(clienteAtualizado.getNome());
			cliente.setCpf(clienteAtualizado.getCpf());
			clientesRepository.save(cliente); 	
		}
	}
}
