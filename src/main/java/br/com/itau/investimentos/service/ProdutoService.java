package br.com.itau.investimentos.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import java.util.Optional;
import br.com.itau.investimentos.model.Produto;
import br.com.itau.investimentos.repository.ProdutoRepository;

@Service
public class ProdutoService {

	
	@Autowired
	private ProdutoRepository produtoRepository;
	
	public Iterable<Produto> obterProdutos() {
		System.out.println("Chamaram o listar produtos");

		return produtoRepository.findAll();
	}
	
//	public Iterable<AplicacaoDTO> obterAplicacoes(Long produtoId) {
//		return aplicacaoRepository.findAllByProduto_Id(produtoId);
//	}
	
	public void criarProduto(Produto produto) {
		produtoRepository.save(produto);
		System.out.println("Chamaram o criar do " + produto.getNome());
	}

	public void apagarProduto(Long id) {
		Produto produto = encontraOuDaErro(id);
		produtoRepository.delete(produto);
	}

	public Produto editarProduto(Long id, Produto produtoAtualizado) {
		Produto produto = encontraOuDaErro(id);
		
		produto.setNome(produtoAtualizado.getNome());
		produto.setRendimento(produtoAtualizado.getRendimento());
		
		return produtoRepository.save(produto);
	}
	
	public Produto encontraOuDaErro(Long id) {
		Optional<Produto> optional = produtoRepository.findById(id);
		
		if(!optional.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Produto não encontrado");
		}
		
		return optional.get();
	}

}
