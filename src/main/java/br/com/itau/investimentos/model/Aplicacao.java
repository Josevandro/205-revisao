package br.com.itau.investimentos.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "aplicacoes")
public class Aplicacao {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne(cascade = CascadeType.MERGE)
	private Clientes cliente;

	@ManyToOne(cascade = CascadeType.MERGE)
	private Produto produto;

	@Column
	private Double valor;
	
	@Column
	private Integer meses;
}
