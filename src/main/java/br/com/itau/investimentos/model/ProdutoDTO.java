package br.com.itau.investimentos.model;

import java.util.List;

public interface ProdutoDTO {

	Long getId();
	
	String getNome();

	Long getRendimento();
	
	List<AplicacaoDTO> getAplicacoes();

}
