package br.com.itau.investimentos.model;

import java.util.List;

public interface ClientesDTO {
	
	Long getId();
	
	String getNome();

	int getIdade();
	
	List<AplicacaoDTO> getAplicacoes();

}
