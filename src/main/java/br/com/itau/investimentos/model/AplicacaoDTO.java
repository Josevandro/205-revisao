package br.com.itau.investimentos.model;

public interface AplicacaoDTO {

	Long getId();
	
	Double getValor();
	
	Integer getMeses();

}
