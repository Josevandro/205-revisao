package br.com.itau.investimentos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.investimentos.model.Clientes;
import br.com.itau.investimentos.service.ClienteService;

@RestController
public class ClientesController {
	@Autowired
	ClienteService clienteService;
	
	@PostMapping("/cliente")
	@ResponseStatus(code = HttpStatus.CREATED)
	public void cadastrarCliente(@RequestBody Clientes cliente) {
		clienteService.cadastrarCliente(cliente);
	}
	
	@GetMapping("/cliente")
	public Iterable<Clientes> consultarClientes() {
		return clienteService.consultarClientes();
	}
	
	@DeleteMapping("/cliente/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void deletarCliente(@PathVariable Long id) {
		clienteService.deletarCliente(id);
	}
	
	@PatchMapping("/cliente/{id}")
	public void editarCliente(@PathVariable Long id, @RequestBody Clientes cliente) {
		clienteService.editarCliente(id, cliente);
	}
}
