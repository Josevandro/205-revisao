package br.com.itau.investimentos.controller;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.itau.investimentos.model.Produto;
import br.com.itau.investimentos.service.ProdutoService;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = {ProdutoController.class})

public class ProdutoControllerTest {

	
	@MockBean
	ProdutoService produtoService;
	
	@Autowired
	MockMvc mockMvc;
	
	@Test
	public void deveObterListadeProdutos() throws Exception{
		Produto produto = new Produto();
		produto.setId(1L);
		produto.setNome("Ações");
		produto.setRendimento(0.0005);
		
		List<Produto> produtos = Lists.newArrayList(produto);

		Mockito.when(produtoService.obterProdutos()).thenReturn(produtos);
		
		mockMvc.perform(MockMvcRequestBuilders.get("/produto"))
		.andExpect(status().isOk())
		.andExpect(content().string(containsString("1")))
		.andExpect(content().string(containsString("Ações")));
//		.andExpect(content().string(containsDouble(0.0005)));
	}
	
	@Test
	public void deveApagarUmProduto() throws Exception{
		Produto produto = new Produto();
		produto.setId(1L);
		produto.setNome("Ações");
		produto.setRendimento(0.0006);
		
		mockMvc.perform(MockMvcRequestBuilders.delete("/produto/1"))
		.andExpect(status().isNoContent());

		
		Mockito.verify(produtoService).apagarProduto(produto.getId());
		
	}
	
	@Test
	public void deveCriarUmProduto() throws JsonProcessingException, Exception{
		
		Produto produto = new Produto();
		produto.setId(1L);
		produto.setNome("Previdência");
		produto.setRendimento(0.0005);
		
		//List<produto> produtos = Lists.newArrayList(produto);

		ObjectMapper objectMapper = new ObjectMapper();
		
		mockMvc.perform(MockMvcRequestBuilders.post("/produto")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(objectMapper.writeValueAsString(produto)))
				.andExpect(status().isCreated());
		
		Mockito.verify(produtoService).criarProduto(Mockito.any(Produto.class));
		
	}

	@Test
	public void deveEditarUmProduto() throws JsonProcessingException, Exception{
		
		Produto produto = new Produto();
		produto.setId(1L);
		produto.setNome("CDB");
		produto.setRendimento(0.0005);

		ObjectMapper objectMapper = new ObjectMapper();
		
		mockMvc.perform(MockMvcRequestBuilders.patch("/produto/1")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(objectMapper.writeValueAsString(produto)))
				.andExpect(status().isOk());
		
		Mockito.when(produtoService.editarProduto(1L, produto)).thenReturn(produto);
		
	}

}
