package br.com.itau.investimentos.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;

import br.com.itau.investimentos.model.Produto;
import br.com.itau.investimentos.repository.ProdutoRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {ProdutoService.class})

public class ProdutoServiceTest {
	@Autowired
	ProdutoService produtoService;
	
	@MockBean
	ProdutoRepository produtoRepository;
	
	@Test
	public void deveListarTodosOsProdutos() {

		//setup
		Produto produto = new Produto();
		
		List<Produto> produtos = new ArrayList<>();
		
		produtos.add(produto);
		
		Mockito.when(produtoRepository.findAll()).thenReturn(produtos);
		
		//action
		//Iterable<Produto> resultado = ProdutoService.obterProdutos();
		List<Produto> resultado = Lists.newArrayList(produtoService.obterProdutos());
		
		//check
		//Iterator<Produto> iterador = resultado.iterator();
		//Produto ProdutoResultado = iterador.next();
		//assertNotNull(ProdutoResultado);
		assertNotNull(resultado.get(0));
		assertEquals(produto, resultado.get(0));
		
	}
	
//	@Test
//	public void deveListarTodosOsProdutosDeUmCliente() {
//		
//		//Setup
//		long id = 1;
//		
//		ProdutoDTO ProdutoDTO = Mockito.mock(ProdutoDTO.class);
//		List<ProdutoDTO> Produtos = new ArrayList<>();
//		Produtos.add(ProdutoDTO);
//		
//		Mockito.when(ProdutoRepository.findAllByDiretor_Id(id)).thenReturn(Produtos);
//		
//		//Action
//		List<ProdutoDTO> resultado = Lists.newArrayList(ProdutoService.obterProdutos(id));
//		
//		//check
//		assertEquals(ProdutoDTO, resultado.get(0));
//		
//	}

	@Test
	public void deveCriarUmProduto() {
		//setup
		Produto produto = new Produto();
		
		//action
		produtoService.criarProduto(produto);
		
		//check
		Mockito.verify(produtoRepository).save(produto);
		
	}
	
	@Test
	public void deveEditarUmProduto() {
		//setup
		Long id = 1L;
		Long novoId = 2L;
		String novoNome = "Ações";
		Double novoRendimento = 0.0005;
		
		Produto produtoDoBanco = criarProduto(id);
		Produto produtoDoFront = criarProduto(id);
		
		//produtoDoFront.setId(novoId);
		produtoDoFront.setNome(novoNome);
		produtoDoFront.setRendimento(novoRendimento);
		
		Mockito.when(produtoRepository.findById(id)).thenReturn(Optional.of(produtoDoBanco));
		Mockito.when(produtoRepository.save(produtoDoBanco)).thenReturn(produtoDoBanco);
		
		
		//action
		Produto resultado = produtoService.editarProduto(id, produtoDoFront);
		
		//check
		Mockito.verify(produtoRepository).save(produtoDoBanco);
		assertNotNull(resultado);
		assertEquals(novoNome, resultado.getNome());
		assertEquals(novoRendimento, resultado.getRendimento());
		assertEquals(id, resultado.getId());
	}

	@Test
	public void deveApagarUmProduto() {
		//setup	
		Long id = 1L;
		
		Produto Produto = new Produto();

		Mockito.when(produtoRepository.findById(id)).thenReturn(Optional.of(Produto));		

		//action
		produtoService.apagarProduto(id);

		//check
		Mockito.verify(produtoRepository).delete(Produto);
	}
	
	@Test(expected = ResponseStatusException.class)
	public void deveLancarExcecaoQuandoNaoEncontrarOProduto() {
		//setup
		Long id = 1L;
		Long novoId = 2L;
		
		Produto ProdutoDoFront = criarProduto(novoId);
				
		Mockito.when(produtoRepository.findById(id)).thenReturn(Optional.empty());
		
		//action
		Produto resultado = produtoService.editarProduto(id, ProdutoDoFront);
		
		//check
		//Mockito.verify(ProdutoRepository).save(ProdutoDoBanco);

	}

	private Produto criarProduto(long id) {
		Produto produto = new Produto();
		produto.setId(id);
		produto.setNome("Previdência");
		produto.setRendimento(0.0006);	
		return produto;
	}

}
